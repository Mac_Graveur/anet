# ANET

## Mise en données des informations sur l'appel à projet Archivage numérique en Territoires (ANET)

Le **Service interministériel des archives de France** soutient financièrement des projets d'archivage électronique. L'ANET fait suite au dispositif AD-ESSOR (2014-2019). Vous trouverez sur le site [France archives](https://francearchives.fr/fr/article/171593987) toutes les informations sur cet appel à projet.


Dans le répertoire projet vous trouverez le jeu de données qui a permis de réaliser différentes visualisation des informations sur les projets soutenus par le SIAF, ainsi que quelques réalisations.


Une carte a aussi été créé via l'outil en ligne **Carto**. Elle est disponible [en ligne](https://frama.link/carte_anet)

